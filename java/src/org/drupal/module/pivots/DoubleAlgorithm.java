package org.drupal.module.pivots;

import java.util.Map;
import java.util.Properties;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.io.StringWriter;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Nov 23, 2007
 */
public class DoubleAlgorithm extends Algorithm {
    protected DoubleAlgorithm(int pivotId) throws Exception {
        super(pivotId);
    }

    protected int basePivot;
    protected int lastUpdate;

    public void index() throws Exception {
        logger.info("Attention: the current implementation will do full reindex in an incremental index cycle");
        reindex();
    }

    public void reindex() throws Exception {
        logger.info("Start double pivot processing.");
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("DELETE FROM pivots_match WHERE pivot_id="+pivotId);
        stmt.close();

        String tableName = "_pivots_matchx_"+pivotId;
        stmt = connection.createStatement();
        stmt.executeUpdate("CREATE TEMPORARY TABLE IF NOT EXISTS " + tableName +
                " (src_id INT(11) UNSIGNED NOT NULL, dest_id INT(11) UNSIGNED NOT NULL)" +
                " SELECT DISTINCT src_id, dest_id FROM pivots_match m INNER JOIN node n" +
                " ON m.src_id=n.nid WHERE pivot_id="+basePivot+" AND n.type<>'forum'");
        stmt.close();

        genericDouble(tableName, "src_id", "dest_id", Operation.SIMPLE_TEMP_TABLE);

        lastUpdate = SystemUtils.getInstance().currentUnixTime(connection);
        logger.info("End double pivot processing");
    }

    public void configure(Map parameters) throws IllegalArgumentException {
        basePivot = Integer.parseInt((String)parameters.get("basePivot"));
        try {
            lastUpdate = Integer.parseInt((String)parameters.get("lastUpdate"));
        } catch (Exception e) {
            lastUpdate = 0;
        }
    }

    protected void updateParameters() throws Exception {
        Properties states = new Properties();
        states.setProperty("lastUpdate", (new Integer(lastUpdate)).toString());
        StringWriter writer = new StringWriter();
        states.store(writer, null);
        writer.close();
        String statesString = writer.toString();

        Statement stmt = connection.createStatement();
        stmt.executeUpdate("DELETE FROM pivots_external WHERE pid="+pivotId);
        stmt.close();
        PreparedStatement stmt1 = connection.prepareStatement("INSERT INTO pivots_external(pid, states) VALUES(?, ?)");
        stmt1.setInt(1, pivotId);
        stmt1.setString(2, statesString);
        stmt1.executeUpdate();
        stmt1.close();
    }
}
