/**
 * Define useful functions.
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Nov 22, 2007
 */
package org.drupal.module.pivots;

import java.io.FileReader;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

public class SystemUtils {
    // version
    public final String VERSION = "0.0.4";

    // singleton
    private static SystemUtils ourInstance = new SystemUtils();
    public static SystemUtils getInstance() {
        return ourInstance;
    }
    private SystemUtils() {
    }

    // logging
    private Logger logger;
    public Logger getLogger() {
        if (logger==null) {
            logger = Logger.getLogger(this.getClass().getPackage().getName());
        }
        return logger;
    }

    // settings
    private String dbUrl;
    private String dbPrefix; // TODO: drupal db prefix
    public void loadSettings() throws Exception {
        Properties settings = new Properties();
        settings.load(new FileReader("config.properties"));
        dbUrl = settings.getProperty("dbUrl");
        Class.forName("com.mysql.jdbc.Driver");
    }

    // database connection
    public Connection getConnection() throws Exception {
        // TODO: might use connection pooling
        Connection conn = DriverManager.getConnection(dbUrl);
        return conn;
    }
    public void closeConnection(Connection conn) throws Exception {
        // TODO: if using pooling, this might be return the conn to the pool.
        if (conn != null) {
            conn.close();
        }
    }

    // database utilities
    public boolean tableExists(Connection conn, String tableName) throws Exception {
        DatabaseMetaData metaData = conn.getMetaData();
        ResultSet rs = metaData.getTables(null, null, tableName, null);
        boolean exists;
        if (rs.next()) {
            exists = true;
        } else {
            exists = false;
        }
        rs.close();
        return exists;
    }
    public int currentUnixTime(Connection conn) throws Exception {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT unix_timestamp()");
        rs.next();
        int utime = rs.getInt(1);
        rs.close();
        stmt.close();
        return utime;
    }

    // other functions
    public void setupEnvironment() throws Exception {
        Connection conn = getConnection();
        if (!tableExists(conn, "pivots_external")) {
            conn.createStatement().executeUpdate(
                    "CREATE TABLE IF NOT EXISTS pivots_external "
                    +"(pid INT(11) UNSIGNED NOT NULL PRIMARY KEY REFERENCES pivots(pid) ON DELETE CASCADE ON UPDATE CASCADE, "
                    +"states VARCHAR(1000) NULL, "
                    +"timestamp TIMESTAMP)");
        }
        conn.close();
    }
}
