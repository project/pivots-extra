package org.drupal.module.pivots;

import org.phprpc.util.PHPSerializer;

import java.util.ArrayList;
import java.util.Map;
import java.sql.PreparedStatement;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Nov 22, 2007
 */
public class PHPForumAlgorithm extends ForumAlgorithm {
    protected PHPForumAlgorithm(int pivotId) throws Exception {
        super(pivotId);
    }

    private Map parameters;

    public void configure(Map parameters) throws IllegalArgumentException {
        this.parameters = parameters;
        targetType = (String) parameters.get("target_type");
        if (targetType==null) throw new IllegalArgumentException();

        magicWord = (ArrayList<String>)parameters.get("magicword");
        if (magicWord==null || magicWord.isEmpty()) throw new IllegalArgumentException();

        enableAlias = (((Byte)parameters.get("enable_alias"))==1);

        try {
            startMark = (Integer)parameters.get("start_mark");
        } catch (Exception e) {
            startMark = 0;
        }
    }

    protected void updateParameters() throws Exception {
        parameters.put("start_mark", new Integer(startMark));
        String arguments = new String(PHPSerializer.serialize(parameters));
        PreparedStatement stmt = connection.prepareStatement("UPDATE pivots SET arguments=? WHERE pid=?");
        stmt.setInt(2, pivotId);
        stmt.setString(1, arguments);
        stmt.executeUpdate();
        stmt.close();
    }
}
