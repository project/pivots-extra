/**
 * This project follows the "eXtreme Programming" practice, where code will change a lot.
 * We will beautify the code later.
 * Please expect the code to be changed frequently.
 */

package org.drupal.module.pivots;

/**
 * Functions of this class:
 *   - Parse command line options
 *   - Read configuration files
 *   - Create algorithm and run it
 *
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Nov 22, 2007
 */

public class Console {

    private static int pivotId;
    private static Action action;
    private static enum Action {INDEX, REINDEX}

    public static void main(String[] args) throws Exception {
        SystemUtils sys = SystemUtils.getInstance();
        sys.getLogger().info("PIVOTS (JAVA) VERSION: " + sys.VERSION);
        sys.loadSettings();
        sys.setupEnvironment();
        parseCommand(args);
        Algorithm algorithm = Algorithm.create(pivotId);
        switch (action) {
            case INDEX:
                algorithm.index();
                break;
            case REINDEX:
                algorithm.reindex();
                break;
        }
        algorithm.cleanup();
        sys.getLogger().info("Process accomplished.");
    }

    private static void parseCommand(String[] args) {
        // BUG: assume args length is 1
        pivotId = Integer.parseInt(args[0]);
        if (args.length>1 && args[1].equalsIgnoreCase("--reindex")) {
            action = Action.REINDEX;
        } else {
            action = Action.INDEX;
        }
    }
}
