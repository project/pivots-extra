package org.drupal.module.pivots;

import org.phprpc.util.PHPSerializer;

import java.util.Map;
import java.sql.PreparedStatement;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Nov 23, 2007
 */
public class PHPDoubleAlgorithm extends DoubleAlgorithm {
    protected PHPDoubleAlgorithm(int pivotId) throws Exception {
        super(pivotId);
    }

    Map parameters;

    public void configure(Map parameters) throws IllegalArgumentException {
        this.parameters = parameters;
        basePivot = Integer.parseInt((String)parameters.get("base_pivot"));
        try {
            lastUpdate = Integer.parseInt((String)parameters.get("last_update"));
        } catch (Exception e) {
            lastUpdate = 0;
        }
    }

    protected void updateParameters() throws Exception {
        parameters.put("last_update", new Integer(lastUpdate));
        String arguments = new String(PHPSerializer.serialize(parameters));
        PreparedStatement stmt = connection.prepareStatement("UPDATE pivots SET arguments=? WHERE pid=?");
        stmt.setInt(2, pivotId);
        stmt.setString(1, arguments);
        stmt.executeUpdate();
        stmt.close();
    }
}
