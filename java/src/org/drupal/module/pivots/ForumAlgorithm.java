package org.drupal.module.pivots;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.regex.Pattern;
import java.io.StringWriter;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Nov 22, 2007
 */
public class ForumAlgorithm extends Algorithm {

    protected String targetType;
    protected List<String> magicWord = new ArrayList<String>();
    protected int startMark;
    protected int endMark;
    protected boolean enableAlias;
    //protected boolean saveMemory = false;

    protected ForumAlgorithm(int pivotId) throws Exception {
        super(pivotId);
    }

    public void index() throws Exception {
        logger.warning("Pivot index processing start time: " + new Date());
        endMark = SystemUtils.getInstance().currentUnixTime(connection);
        logger.info("Start Mark: " + startMark);
        logger.info("End Mark: " + endMark);

        Statement stmt;
        stmt = connection.createStatement();
        stmt.executeUpdate(
                "DELETE FROM pivots_match WHERE pivot_id="+pivotId+" AND " +
                "(src_id NOT IN (SELECT nid FROM node ORDER BY nid) OR dest_id NOT IN (SELECT nid FROM node ORDER BY nid) " +
                "OR a1 NOT IN (SELECT cid FROM comments UNION SELECT 0))");
        stmt.close();
        stmt = connection.createStatement();
        stmt.executeUpdate(
                "DELETE FROM pivots_match WHERE pivot_id="+pivotId+" AND " +
                "(src_id IN (SELECT nid FROM node WHERE type IN ('forum', '"+targetType+"') AND changed>"+startMark+" AND changed<="+endMark+" ORDER BY nid) " +
                "OR dest_id IN (SELECT nid FROM node WHERE type IN ('forum', '"+targetType+"') AND changed>"+startMark+" AND changed<="+endMark+" ORDER BY nid) " +
                "OR a1 IN (SELECT cid FROM comments WHERE timestamp>"+startMark+" AND timestamp<="+endMark+" ORDER BY cid))");
        stmt.close();
        if (!enableAlias) {
            stmt = connection.createStatement();
            stmt.executeUpdate("DELETE FROM pivots_match WHERE pivot_id="+pivotId+" AND a2="+20);
            stmt.close();
        }

        String targetQuery;
        String forumQuery;

        logger.info("Indexing new target nodes with all forum nodes and comments");
        targetQuery =
                "SELECT nid, title, 0 FROM node WHERE type='"+targetType+"' AND changed>"+startMark+" AND changed<="+endMark;
        if (enableAlias) {
            targetQuery += " UNION ALL ";
            targetQuery +=
                "SELECT n.nid, alias, 1 FROM node n INNER JOIN pivots_alias a ON n.nid=a.nid WHERE n.type='"+targetType+"' AND " +
                "((n.changed>"+startMark+" AND n.changed<="+endMark+") OR " +
                "(a.timestamp>"+startMark+" AND a.timestamp<="+endMark+"))";
        }
        forumQuery =
                "SELECT n.nid nid, 0 cid, n.changed score, n.title subject, r.body body FROM node n INNER JOIN node_revisions r " +
                "ON n.nid=r.nid AND n.vid=r.vid WHERE n.type='forum' AND n.changed<="+endMark+ " UNION ALL " +
                "SELECT c.nid, c.cid, c.timestamp score, c.subject subject, c.comment body FROM comments c INNER JOIN node n " +
                "ON c.nid=n.nid WHERE n.type='forum' AND c.timestamp<="+endMark;
        /*if (!saveMemory) matching(targetQuery, forumQuery);
        else matchingSaveMemory(targetQuery, forumQuery);*/
        matching(targetQuery, forumQuery);

        logger.info("Indexing old target nodes with new forum nodes");
        targetQuery =
                "SELECT nid, title, 0 FROM node WHERE type='"+targetType+"' AND changed<="+startMark;
        if (enableAlias) {
            targetQuery += " UNION ALL ";
            targetQuery +=
                "SELECT n.nid, alias, 1 FROM node n INNER JOIN pivots_alias a ON n.nid=a.nid WHERE n.type='"+targetType+"' AND " +
                "(n.changed<="+startMark+" OR a.timestamp<="+startMark+")";
        }
        forumQuery =
                "SELECT n.nid nid, 0 cid, n.changed score, n.title subject, r.body body FROM node n INNER JOIN node_revisions r " +
                "ON n.nid=r.nid AND n.vid=r.vid WHERE n.type='forum' AND changed>"+startMark+" AND changed<="+endMark+" UNION ALL " +
                "SELECT c.nid, c.cid, c.timestamp score, c.subject subject, c.comment body FROM comments c INNER JOIN node n " +
                "ON c.nid=n.nid WHERE n.type='forum' AND c.timestamp>"+startMark+" AND c.timestamp<="+endMark;
        /*if (!saveMemory) matching(targetQuery, forumQuery);
        else matchingSaveMemory(targetQuery, forumQuery);*/
        matching(targetQuery, forumQuery);

        startMark = endMark;
        logger.warning("Pivot index processing end time: " + new Date());
    }

    private static class TargetRecord {
        int nid;
        String title;
        boolean isAlias;
    }
    private static class ForumRecord {
        int forumId;
        int commentId;
        float score;
        String subject;
        String body;
    }

    private void matching(String targetQuery, String forumQuery) throws Exception {
        Statement stmt;
        // retrieve items and forum threads into memory for faster processing
        List<TargetRecord> targetRecordList = new ArrayList<TargetRecord>();
        stmt = connection.createStatement();
        ResultSet targetResult = stmt.executeQuery(targetQuery);
        while (targetResult.next()) {
            TargetRecord targetRecord = new TargetRecord();
            targetRecord.nid = targetResult.getInt(1);
            targetRecord.title = targetResult.getString(2);
            targetRecord.isAlias = targetResult.getBoolean(3);
            targetRecordList.add(targetRecord);
        }
        stmt.close();
        if (targetRecordList.size()==0) return;

        List<ForumRecord> forumRecordList = new ArrayList<ForumRecord>();
        stmt = connection.createStatement();
        ResultSet forumResult = stmt.executeQuery(forumQuery);
        while (forumResult.next()) {
            ForumRecord forumRecord = new ForumRecord();
            forumRecord.forumId = forumResult.getInt("nid");
            forumRecord.commentId = forumResult.getInt("cid");
            forumRecord.score = forumResult.getFloat("score");
            forumRecord.subject = forumResult.getString("subject");
            forumRecord.body = forumResult.getString("body");
            forumRecordList.add(forumRecord);
        }
        stmt.close();
        if (forumRecordList.size()==0) return;

        int count = 1;
        for (TargetRecord tr : targetRecordList) {
            logger.fine("Starting processing for the "+(count++)+"/"+targetRecordList.size()+" item: "+tr.nid);
            if (tr.isAlias) {
                StringTokenizer aliases = new StringTokenizer(tr.title, "|");
                while (aliases.hasMoreTokens()) {
                    Pattern pattern = Pattern.compile("\\b"+Pattern.quote(aliases.nextToken())+"\\b", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
                    for (ForumRecord fr : forumRecordList) {
                        if (pattern.matcher(fr.subject).find() || pattern.matcher(fr.body).find()) {
                            insertMatch(tr.nid, fr.forumId, fr.commentId, fr.score, 20);
                            insertMatch(fr.forumId, tr.nid, fr.commentId, fr.score, 20);
                        }
                    }
                }
            } else {
                String targetTitle = Pattern.quote(tr.title);
                for (String aMagicWord : magicWord) {
                    Pattern pattern;
                    if (aMagicWord.equals("")) {
                        pattern = Pattern.compile("\\b"+targetTitle+"\\b", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
                    } else {
                        aMagicWord = Pattern.quote(aMagicWord);
                        pattern = Pattern.compile(
                                "(?:\\b"+targetTitle+"\\b\\W+?\\b"+aMagicWord+"\\b)|(?:\\b"+aMagicWord+"\\b\\W+?\\b"+targetTitle+"\\b)",
                                Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
                    }
                    for (ForumRecord fr : forumRecordList) {
                        if (pattern.matcher(fr.subject).find() || pattern.matcher(fr.body).find()) {
                            insertMatch(tr.nid, fr.forumId, fr.commentId, fr.score, 10);
                            insertMatch(fr.forumId, tr.nid, fr.commentId, fr.score, 10);
                        }
                    }
                }
            }
        }
    }

    // in the save memory mode, we won't retrieve the table into memory.
    // rather, we'll use the ResultSet cursor.
    /*private void matchingSaveMemory(String targetQuery, String forumQuery) throws Exception {
        logger.warning("Algorithm switching to memory saving mode");
        ResultSet targetResult = connection.createStatement().executeQuery(targetQuery);
        ResultSet forumResult = connection.createStatement().executeQuery(forumQuery);
        while (targetResult.next()) {
            int targetId = targetResult.getInt(1);
            String targetTitle = targetResult.getString(2);
            logger.fine("Starting processing for the "+targetResult.getRow()+"(th) item: "+targetId);
            for (String aMagicWord : magicWord) {
                Pattern pattern;
                if (aMagicWord.equals("")) {
                    pattern = Pattern.compile("\\b"+targetTitle+"\\b", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
                } else {
                    pattern = Pattern.compile(
                            "(?:\\b"+targetTitle+"\\b\\W+?\\b"+aMagicWord+"\\b)|(?:\\b"+aMagicWord+"\\b\\W+?\\b"+targetTitle+"\\b)",
                            Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
                }
                forumResult.beforeFirst();
                while (forumResult.next()) {
                    String subject = forumResult.getString("subject");
                    String body = forumResult.getString("body");
                    if (pattern.matcher(subject).find() || pattern.matcher(body).find()) {
                        int forumId = forumResult.getInt("nid");
                        int commentId = forumResult.getInt("cid");
                        float score = forumResult.getFloat("score");
                        insertMatch(targetId, forumId, commentId, score, 10);
                        insertMatch(forumId, targetId, commentId, score, 10);
                    }
                }
            }
        }
    }*/

    private void insertMatch(int targetId, int forumId, int commentId, float score, int type) throws Exception {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT score FROM pivots_match WHERE " +
                "src_id="+targetId+" AND dest_id="+forumId+" AND pivot_id="+pivotId+" AND a1="+commentId+" AND a2="+type);
        if (rs.next()) {
            float existingScore = rs.getFloat("score");
            if (score>existingScore) {
                Statement stmt1 = connection.createStatement();
                stmt1.executeUpdate("UPDATE pivots_match SET score="+score+
                        " WHERE src_id="+targetId+" AND dest_id="+forumId+" AND pivot_id="+pivotId+" AND a1="+commentId+" AND a2="+type);
                stmt1.close();
            }
        } else {
            Statement stmt2 = connection.createStatement();
            stmt2.executeUpdate("INSERT pivots_match(src_id, dest_id, pivot_id, score, a1, a2) VALUES(" +
                    targetId+", "+forumId+", "+pivotId+", "+score+", "+commentId+", "+type+")");
            stmt2.close();
        }
        stmt.close();
    }

    public void reindex() throws Exception {
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("DELETE FROM pivots_match WHERE pivot_id="+pivotId);
        stmt.close();
        startMark = 0;
        logger.info("Re-indexing.");
        index();
    }

    public void configure(Map parameters) throws IllegalArgumentException {
        targetType = (String)parameters.get("targetType");
        if (targetType==null) throw new IllegalArgumentException();

        String magicWordString = (String)parameters.get("magicWord");
        if (magicWordString!=null && !magicWordString.equals("")) {
            StringTokenizer tokens = new StringTokenizer(magicWordString, "|");
            while (tokens.hasMoreTokens()) magicWord.add(tokens.nextToken());
        } else {
            magicWord.add(""); // null string
        }
        // enableAlias indicates whether to use alias in the matching.
        enableAlias = Boolean.parseBoolean((String) parameters.get("enableAlias"));
        // saveMemory is an indicator for the algorithm to trade performance for memory.
        //saveMemory = Boolean.parseBoolean((String) parameters.get("saveMemory"));
        // startMark is where the indexing should start.
        try {
            startMark = Integer.parseInt((String)parameters.get("startMark"));
        } catch (Exception e) {
            startMark = 0;
        }
    }

    protected void updateParameters() throws Exception {
        Properties states = new Properties();
        states.setProperty("startMark", (new Integer(startMark)).toString());
        StringWriter writer = new StringWriter();
        states.store(writer, null);
        writer.close();
        String statesString = writer.toString();

        Statement stmt = connection.createStatement();
        stmt.executeUpdate("DELETE FROM pivots_external WHERE pid="+pivotId);
        stmt.close();
        PreparedStatement stmt1 = connection.prepareStatement("INSERT INTO pivots_external(pid, states) VALUES(?, ?)");
        stmt1.setInt(1, pivotId);
        stmt1.setString(2, statesString);
        stmt1.executeUpdate();
        stmt1.close();
    }

}
