package org.drupal.module.pivots;

import org.phprpc.util.PHPSerializer;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @auther Daniel Zhou (danithaca@gmail.com)
 * @organization School of Information, University of Michigan
 * Date: Nov 22, 2007
 */
public abstract class Algorithm {

    abstract public void index() throws Exception;
    abstract public void reindex() throws Exception;
    abstract public void configure(Map parameters) throws IllegalArgumentException;
    abstract protected void updateParameters() throws Exception;

    protected final int pivotId;
    protected final Logger logger;
    protected final Connection connection;

    protected Algorithm(int pivotId) throws Exception {
        this.pivotId = pivotId;
        this.logger = SystemUtils.getInstance().getLogger();
        this.connection = SystemUtils.getInstance().getConnection();
    }

    public static Algorithm create(int pivotId) throws Exception {
        Connection conn = SystemUtils.getInstance().getConnection();
        ResultSet rs = conn.createStatement().executeQuery(
                "SELECT arguments, states, alg_name FROM pivots p LEFT JOIN pivots_external e ON p.pid=e.pid WHERE p.pid="+pivotId);
        rs.first();

        String arguments = rs.getString(1);
        String statesString = rs.getString(2);
        String algName = rs.getString(3);
        if (statesString == null) statesString = "";
        Map parameters = retrieveParameters(algName, arguments, statesString);

        SystemUtils.getInstance().closeConnection(conn);

        Algorithm algorithm = null;
        if (algName.equals("local")) {
            String algorithmId = (String)parameters.get("algorithm");
            if (algorithmId.equals("forum")) {
                algorithm = new ForumAlgorithm(pivotId);
            } else if (algorithmId.equalsIgnoreCase("double")) {
                algorithm = new DoubleAlgorithm(pivotId);
            } else {
                throw new IllegalArgumentException("Algorithm ID ("+algorithmId+") not recognized.");
            }
        } else if (algName.equals("forum")) {
            algorithm = new PHPForumAlgorithm(pivotId);
        } else if (algName.equals("double")) {
            algorithm = new PHPDoubleAlgorithm(pivotId);
        } else {
            throw new IllegalArgumentException("Algorithm ID ("+algName+") not recognized.");
        }

        algorithm.configure(parameters);
        return algorithm;
    }

    private static Map retrieveParameters(String algName, String arguments, String statesString) throws Exception {
        Map param = new HashMap();
        Map argumentsParam = (HashMap) PHPSerializer.unserialize(arguments.getBytes());
        if (algName.equals("local")) {
            String paramString = (String)argumentsParam.get("parameters");
            Properties embedParam = new Properties();
            embedParam.load(new StringReader(paramString));
            param.putAll(embedParam);
        } else {
            param.putAll(argumentsParam);
        }
        if (statesString != null) {
            Properties internalStates = new Properties();
            internalStates.load(new StringReader(statesString));
            param.putAll(internalStates);
        }
        return param;
    }

    public void cleanup() throws Exception {
        updateParameters();
        connection.close();
    }

    protected enum Operation {SIMPLE, SIMPLE_TEMP_TABLE};

    protected void genericDouble(String tableName, String itemField, String viaField, Operation op) throws Exception {
        switch (op) {
            case SIMPLE:
            case SIMPLE_TEMP_TABLE:
                // if the table is temporary, we have to create a new temp table because of MySQL limitation
                // that temporary table can not be reopen in the same query.
                // but it's still better than creating a physical table.
                String shadowTableName = tableName;
                if (op == Operation.SIMPLE_TEMP_TABLE) {
                    shadowTableName = tableName + "_shadow";
                    Statement stmt = connection.createStatement();
                    stmt.executeUpdate("CREATE TEMPORARY TABLE IF NOT EXISTS "+shadowTableName+" LIKE "+tableName);
                    stmt.close();
                    stmt = connection.createStatement();
                    stmt.executeUpdate("INSERT INTO "+shadowTableName+" SELECT * FROM "+tableName);
                    stmt.close();
                }
                Statement stmt = connection.createStatement();
                stmt.executeUpdate("INSERT INTO pivots_match("+itemField+", "+viaField+", pivot_id, score) "+
                        "SELECT a."+itemField+", b."+itemField+", "+pivotId+", count(a."+viaField+") FROM "
                        +tableName+" a INNER JOIN "+shadowTableName+" b ON a."+viaField+"=b."+viaField
                        +" GROUP BY a."+itemField+", b."+itemField);
                stmt.close();
                break;
        }
    }
}
