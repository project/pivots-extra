package org.phprpc.util;

import java.util.HashMap;

public class TryPHPSerializer {
    public static void main(String[] args) throws Exception {
        String s1 = "a:3:{s:5:\"limit\";s:1:\"5\";s:10:\"base_pivot\";s:2:\"10\";s:11:\"last_update\";i:1195705496;}";
        //String s1 = "a:6:{s:5:\"limit\";s:1:\"5\";s:10:\"start_mark\";i:1195702093;s:11:\"target_type\";s:11:\"module_demo\";s:9:\"magicword\";a:3:{i:0;s:6:\"module\";i:1;s:5:\"theme\";i:2;s:6:\"object\";}s:12:\"enable_alias\";i:1;s:11:\"enable_cron\";i:0;}";
        //String s1 = "a:6:{s:5:\"limit\";s:1:\"0\";s:10:\"start_mark\";N;s:11:\"target_type\";s:15:\"project_project\";s:9:\"magicword\";a:1:{i:0;s:0:\"\";}s:12:\"enable_alias\";i:1;s:11:\"enable_cron\";i:0;}";
        byte[] bytes = s1.getBytes();
        Object obj = PHPSerializer.unserialize(bytes);
        HashMap hashMap = (HashMap) obj;
        //System.out.println(hashMap.get("start_mark"));
        String s2 = new String(PHPSerializer.serialize(obj));
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s1.equals(s2));
    }
}
